# NonVisual Desktop Access
![an image](https://www.nvaccess.org/wp-content/uploads/2019/05/NVDA-cover-with-text-2.jpg)
## _Introduction_
NVDA, the free and open source screen reader for Microsoft Windows. It is developed by NV Access in collaboration with a global community of contributors.This software was originaly released in 2006, had it's latest stale realese the 22 February 2022. This project has won awards in the Mozilla Open Source Support Awards event. 
If have chosen this repository to learn more about screen readers and because the fact these softwares are free can help a lot of person.


A screen reader is a form of assistive technology (AT) that renders text and image content as speech or braille output. Screen readers are essential to people who are blind, and are very helpful for people who are visually impaired, illiterate, or have a learning disability.
## _The origins of NVDA_
Michael Curran was blinded at the age of 15. He then realised the high prices of screen readers and starded to code his own on python in 2006. Throughout the years Michael Curran with the help of other contributors expanded his project to more capabilities. The progams were enhanced for web browsing, support for more programs, braille display output, and improved support for more languages. To manage continued development of NVDA, Curran, along with James Teh, founded NV access in 2007. 
## _Code languages used_
NVDA has been coded in many different code languages, here is a list of them :

- Python
- C/C++
- Robot Framework
- Powershell
- NSIS
- Other

## _Github repository_
Here is a peek at their github page.

| github page | Numbers |
| ------ | ------ |
| Contributors | 106 |
| Branches | 654 |
| Issues | 2.6k |

## _Pull request and Issue management_
For each issue, the repository respond to them in the following form :

- _write a summary of the issue_
- _discribe how the pull request fixed the issue_
- _write down their testing strategy_
- _give the known issues with the pull request_

## _Useful links_ 
[NVDA(NonVisual Desktop Access) github page](https://github.com/nvaccess/nvda)
[NV Access](https://www.nvaccess.org/)

















