# Visual Studio Code - Open Source ("Code - OSS")

[<img src="https://upload.wikimedia.org/wikipedia/commons/9/9a/Visual_Studio_Code_1.35_icon.svg" width=20% height=20%>](https://code.visualstudio.com/)

## Table of content
+ What is VS Code ?
+ What is the "Code - OSS" repository ?
+ Languages used on the repository
+ Some statistics

## What is VS Code ?
Visual Studio Code is a source-code editor made by [Microsoft](https://www.microsoft.com/fr-fr) under the MIT license, that can be used with a lot of programming languages.

<img src="https://user-images.githubusercontent.com/35271042/118224532-3842c400-b438-11eb-923d-a5f66fa6785a.png" width=100% height=100%>

VS code is well known to be one the simplest IDE to use while being really completed and updated monthly.
That's why it is the most popular developer environment tool, with 70% of developers using it, according to [Stack Overflow Developper Survey 2021](https://insights.stackoverflow.com/survey/2021#section-most-popular-technologies-integrated-development-environment).

## What is the "Code - OSS" repository ?
VS Code being updated monthly, Microsoft's developers use  ` Code - OSS ` repository to develop the software along side with the community. 

The repository is used to work on code and issues/bugs but also to publish the documentation and plans for next updates. Thus, there are many ways to help in this project : 

+ Submiting bugs and feature requests
+ Reviewing source code changes
+ Reviewing the documentation or adding some

## Some statistics

### A really active repository
|Collaborators|Forks|Stars|Releases|
|---|---|---|---|
|1603|21.8k|129k|62|

### Languages used on the repository
|Languages|Percentage|
|---|---|
|TypeScript|93.8 %|
|JavaScript|3.3 %|
|CSS|1.4 %|
|HTML|0.7 %|
|Inno Setup|0.7 %|
|Shell|0.1 %|
