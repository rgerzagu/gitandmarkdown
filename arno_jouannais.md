# GIT Repository Report
**Visual Code studio** repository (click [here](https://github.com/microsoft/vscode) for the repo's page)
<p align="center">

<img src="https://logowik.com/content/uploads/images/visual-studio-code7642.jpg" width=20% height=20%>

</p>

##  The project 

- [x] *What is it :*  
  
**VS code** is code editor which allows users to also visualize things such as debug windows while coding ; allowing them to code faster in their projects, by having a direct feedback on the same software. 
As the Git page puts it : 
> «  It provides comprehensive code editing, navigation, and understanding support along with lightweight debugging, a rich extensibility model, and lightweight integration with existing tools. »

- [x] *Why I chose it :*  
  
During our previous courses at Enssat, we used this software, which I found easy and pleasant to use. So when I saw that it worked through a GIT repository I did not hesitate to make my report on this project.

## Description of the repository

When we check out the collaborators page on the repo, we realize there is more than a thousand collaborators since 2015 that frequently add or modify thins through commits. As the repo is public anyone can participate, only if respecting the [code of conduct](https://opensource.microsoft.com/codeofconduct/) imposed by the moderators (Microsoft employees). Moreover, the modifications can be about typos, debugging, aspect or almost anything from a *big update* to a *small improvement*.

It appears that specific topics (therefore features or extensions) are not exactly beeing updated with this repository. There are various **related projects** that go along the vscode global repo. As examples they give names such as « node debug », and these have their own repo. This architecture improves the efficiency and reduces the time spent working for the persons helping.

The purpice of this repo is to improve the software based on what users believe is incomplete or not adapted. Usually, there is a roadmap created each 12 months, that leads to multiples explorations. These explorations are then shared and that's where users tend to help the most. Then it it basically raw coding.
For example the latest 2021 roadmap lead to the followings **objectives** (each with various sub-objectives) :

- Improving global security

- Improving performance throughout platforms and workspaces

- Incrementally improve presentation -> that means making sure the API is user-friendly, and that the software is harmonious

- Upgrading the parameters by setting up new possibilities such as customization of workbenches, or extension acceptance

- Many more...

## PR & issue management

#### Pull Request

When it comes to manages multiple providers for a certain part of the code, Git is one of the best tools available. And for this repo ( for all repos but we are talking about this one today), there are [guidelines](https://github.com/microsoft/vscode/blob/1aaf7cd5716edaf45eb8589a0f2f6061407eb03c/CONTRIBUTING.md) to open a good **pull request**.
And as there are already many contributors, its ask to new ones to verify if the problem your about to solve is not already under investigation, in which case you are asked to reply to the existing issue. These type of guidelines are created to keep a clean repo, as when the repo becomes almost always bigger, it needs to have a clean architecture in order not to lose one's self mind when working on it.

One reason to let anyone help, is because there is always someone better than you online when your talking about a specific subject, therefore letting everyone help allows your project to be improved by the bests.

Something interesting is that most PRs are often discussed between users, and their work put in common definitely has precious value for the repo.

#### Issue management

In order to simplify et improve the issue management efficiency, there are labels to classify issues. The labels are not mandatory, any issue can only be a simple issue, but some have labels such as : "translation", "web", "bug", "feature-request", etc. These labels help each contributor as if they are specifically good in resolving a type of issue, they might only select those and work more efficiently. 

<p align="center">

 |My first table|    |    |     |       |            |
 |--------------|----|----|-----|-------|------------|
 | Yes          | It | Is | All | About | Efficiency |

</p>

Also, one classification is milestones. Milestones are set to give developpers a goal to achieve and is created by the moderators to give people dates for updates first and to put forth their willingness to correct bugs and improve the software to always be the very finest.

