# CrazyFlie Firmware

![crazyflie 2.1](https://www.bitcraze.io/images/crazyflie2-1/crazyflie_2.1_585px.jpg)

## Table of contents
1. The project
2. The repository
3. Pull request and issue management

### The project
The **Crazyflie** is an opensource drone created by **Bitcraze** in 2014 that you can code in Python. In addition to being able to fly, it is equipped with various sensors such as accelerometers or gyroscope. You can also add additional modules such as the flow deck to detect objects around it. It can communicate with a low latency radio or with Bluetooth LE. Then, we can control this drone from a PC application "crazyflie PC client" with or without a gamepad or with a phone. The drone firmware is coded in C, so this repository uses mostly C (95.8%).

I chose this project because I have owned this drone for less than a year and I have not yet explored the **opensource** side of it. All the documents of the drone's firmware are in fact present on this github https://github.com/bitcraze/crazyflie-firmware.


### The repository

The objective of this repository is to access the different resources of the drone (firmware,...) and to improve it according to the requests or the problems found by the users. As the repository is public, everyone can participate.

#### The repository in figures

| Contributors | Commits | Releases | Latest Release |
| -------- | -------- | -------- |-----|
| 74     | 2451   | 24    | 2022.01 |

Over a **one-month period** from 19 January to 19 February, 5 contributors have been active on this repository. There was :
* 16 active pull request : 2 open + 14 merged
* 19 active issues : 13 new + 6 closed
* 50 commits to master and 63 commits to all branches (excluding merges)
* On master, 267 files have changed and there have been 30,472 additions and 6,251 deletions
* 1 release

According to this figures, the repository is quite **active**. It continues to have upgrades and updates both on the appearance of new products and in the firmware.

The **documentation** of the drone, the firmware programming and the component diagram or the routing diagram, are also present on this website https://www.bitcraze.io/products/crazyflie-2-1/.

### Pull request and issue management

The repository is divided into two parts. **Issues** which is only there for problems encountered by users. And **pull request** where there are solutions or improvements proposed by users.

|         | Issue | Pull Request |
| -------- | -------- | -------- |
| Open     | 50     | 5     |
| Closed     | 506     | 383     |

In order to simplify and improve the efficiency of problem management and improvement proposals, there are optional labels to classify the different topics. For example, there is the label "bug", "basic functionality" or "documentation" to make it easier for users and contributors.
