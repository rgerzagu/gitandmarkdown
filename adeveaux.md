# **Open Source Repository : AppFlowy**

<p align="center">
<img src="https://ia-petabox.archive.org/services/img/github.com-AppFlowy-IO-appflowy_-_2021-11-21_14-24-45" width=25% height=25%>
</p> 

## **1. The project and why I choose it**

### **The project :**
***AppFlowy*** is a project to create a workspace for people to work on, of the same type as ***Notion***. The creators of this project explain, as  *Notion* has been their "*favorite project and knowledge management tool*", that they wanted to allow people to have the same functionnalities as Notion but wanted to add data security and more compatibility with mobile devices (which are problems Notion is critised for).

### **Their values :** 
- Data privacy first
- Reliable native experience
- Community-driven extensibility 

### **Website :**
They created a website *[AppFlowy](https://www.appflowy.io)* dedicated to their tool, as they call it, to allow people finding links to download AppFlowy and to help people contributing to the project if they want to.

### **The Language they use to code :**
<div style="margin-left: auto;
            margin-right: auto;
            width: 30%">

| Language | Proportion |
| :-----------: | :-----------: |
| Rust | 56,7 % |
| Dart | 37,6 % |
| C++ | 2,4 % |
| CMake | 1,4 % |
| Ruby | 0,4 % |
| Swift | 0,3 % |
| Other | 1,2 % |
</div>

### **Why did I chose this project ?**
I actually use daily ***Notion*** to organise my life, what I got to do, projects I have, etc ... But the fact that it is not mobile devices-friendly is not very practical. So I thought this was an intringuing project.

## **2. The repository**
### **Collaborators and commits**
The creators of this project want people to collaborate with them to help them improving their project.

- This repository has a number of **42 collaborators**. 
- The main collaborator of this project has done **885 commits**. The other collaborators did between 1 and 56 commits. 

The maintainers ask for specific style of commit messages so that everybody can understand each other and it become clearer.
  
### **External documentation**
They created an external documentation on their website (*[AppFlowy](https://www.appflowy.io)*). This documentation is either for installing AppFlowy or contributing to it. In the contribution's part of the documentation, there is everything you need from how to start coding to how to pull request your inputs to the projects.

## **3. Issue management and pull-requests**

### **Issue management**

In the folder *Issues*, we can see that the contributors put labels on it to indicat what are the issues. Moreover, they put a type of issue at the beginning of the "title" of the issue, such as "[BUG]" for example.

### **Pull-requests**

Once people are done working on the part on the code on their local repository, they can ask for a Pull-Request. Every PR that is submitted will be reviewed by one or more AppFlowy maintainers, to ensure everything correspond to what is asked in the part of the project they worked on. When the Pull-Request is accepted, conflicts can happen. Those conflicts can be resolved by the maintainers.
