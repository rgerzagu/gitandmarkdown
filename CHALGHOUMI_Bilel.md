# :computer: **[GRUB2 THEMES](https://github.com/vinceliuice/grub2-themes) PROJECT** :computer:

![GRUB2 BANNER](https://raw.githubusercontent.com/vinceliuice/grub2-themes/master/banner.png)

**The purpose of this document** will be to explain what "GRUB2 THEME" is and what it does. 

**In a second time**, I'll make a description of the repository **then** I'll talk about Pull Request and Issues management. 

**Finally**, I will explain my motivation for choosing this repository. 

***

## :book: **SUMMARY** :book:

1.  [What is GRUB THEMES ?](#what-is-grub-themes-?)
2.  [Repository description](#repository-description)
3.  [What about *PULL REQUEST* and *ISSUES* ?](#)
4.  [Why did I choose this repository ?](#)

***

## :question: **What is GRUB THEMES ?** :question: 

GRUB2 THEMES is a Shell script running on the Linux operating system that allows to change the image of the background and also the interface of the **grub** when starting our computer.

> *Image illustrating the grub :* 

> ![GRUB BOOT](https://doc.ubuntu-fr.org/lib/exe/fetch.php?tok=616545&media=http%3A%2F%2Fpix.toile-libre.org%2Fupload%2Foriginal%2F1353953772.png)

Its principle of working is very simple and is easily customizable according to the desires of everyone :

* We have the choice of icons that will be displayed on the grub.
* We have the choice of the image that we can use in the background.
* We can set the resolution of the grub background.
* We can activate or deactivate the theme of the grub at any time (not permanent).

***

## :bookmark: **Repository description** :bookmark:

| Collaborators | Poll requests | Forks | Releases | Stars | Issues | External documentations|
| ------------- | ------- | ----- | ----- | -------- | ---------------------- | ----------------- |
| 28 | 53 | 149 | 9 | 1.7k | 29| [1 : Reference](http://wiki.rosalab.ru/en/index.php/Grub2_theme_/_reference) -- [2 : Tutorial](http://wiki.rosalab.ru/en/index.php/Grub2_theme_tutorial)

***

## :thinking: **What about *PULL REQUEST* and *ISSUES* ?** :thinking:

Regarding Pull requests and Issues, the owner of the repository invites people who wish to contribute to the project. 

Indeed, to do this, you just have to fork the project, make the modifications on a branch in your name and then make a Pull request.

Similarly, if a problem is detected on the script, just create a New issue in the corresponding tab so that it is resolved. 

***
## :smiley: **Why did I choose this repository ?** :smiley:

*Why did I choose this repository ?*  Simply because since I installed my dual boot on my personal computer, I found the grub interface ugly and I wanted something more beautiful and colorful.

I think that I am not the only one of this opinion and that this small script must be widely democratized.

***

**Made with love  by :heart: Bilel CHALGHOUMI :heart:**
