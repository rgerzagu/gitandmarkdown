# GitAndMarkdown - Evaluation - Thomas PLUTA [awesome-raspberry-pi](https://github.com/thibmaek/awesome-raspberry-pi)
## Introduction

The [awesome-raspberry-pi](https://github.com/thibmaek/awesome-raspberry-pi) repository is full of content about the use of the Raspberry PI which are card-sized single-board computers. There is content for all models of Raspberry PI. This repository aims to help people to find resources to learn about Raspberry PI computers and make little projects.

![Raspberry PI Logo](https://d1yjjnpx0p53s8.cloudfront.net/styles/logo-thumbnail/s3/042017/untitled-1_86.png?itok=HsEx5xca)


All the resources are conforming to the [Awesome Manifesto](https://github.com/sindresorhus/awesome/blob/master/awesome.md). This manifesto which only list awesome project and stuff that you and other users find awesome.


#### My choice:
I chose this repository because all the projects listed in it seem quite entertaining and seem really well made to learn things about Raspberry PI computers.


## Description of the repository
#### Contents of this repository:
- Models
- Tools
- Projects
- OS Images
- Resources

This contents are all related to Rasberry Pi nano-computers.

#### Numbers

|Collaborators|Commit|Forks|Views|
|---|---|---|---|
|119|444|875|450|

 #### Most important contributing rules

- Every pull request must have a useful title. Every pull request that can not be identified easely will be closed right away.
- Only README.md can be modified , no other file is allowed to be modified.
- Add items in the category it belongs and respect alphabetical order.
- Only cources in english are allowed.
- Addings to the [Applications](https://github.com/thibmaek/awesome-raspberry-pi#useful-apps) must be done in the following format: "**App name** - Description goes here. [Android](https://play.google.com/...), [iOS](https://itunes.apple.com/...)"

#### Languages used

|Language|Proportion|
|---|---|
|Shell|66.6%|
|Makefile|18.1%|
|Ruby|15.3%|