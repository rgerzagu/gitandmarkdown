![Tock](Tock.svg)
# Tock/Tock github page
## What is Tock.

Tock is an active and secured **embedded operating system for microcontrollers** designed for multiple application to run together safely on Cortex-M and RISC-Vbased platforms.
Based on a Rust to protect the kernel, it protect specific device drivers and **isolate devices drivers from each others**.
It also use memory protection to keep applications independant from each others.
Tock in an active system that is kept actualised and which provide updates improvements and support.

## Tock gestion on Github

Created 4 years ago, Tock is now on its **2.0 version**, and count many collaborators which lead to a constant and abondant activity (by the time I write this essay, a pull request has been merged) on github:

| Activity                 | Number                 |
|--------------------|--------------------|
|Stars (follow)     | 3.4k|
|Forks|465|
|Commit|10k|
|Total pull requests |2.5k|
|Live Issues|70+|
|Closed Issues|450|
|Live Pull requests|34|
|Workflow runs|12k|


Tock runs a full [documentation](https://github.com/tock/tock/tree/master/doc) where people can find :
- References
- Courses
- Debugging code
- ...

And many **tips** to use and optimize Tock utilisation:
- Compliation guide
- Code size optimisation tips
- Kernel design principles
- Rust lifetimes
- ...

Git utilisation enables on the first hand a clear and detailed view of the system and is functional, which followed by the many tutorial and TIPS pages create an autonomy for users to discover and use the program. On the other hands it allows many people to work at the same time on the optimisation and the updates of the application, that create a security on what is pushing on the codes.

You can also check [Tock website](www.tockos.org) to get more informations or just to get started.


## Pull request gestion
As most of collaborating projects, Tock manages many **pull requests** everyday, which lead to an ordered gestion. Pull requests are sorted at any levels, containing **labels** that enumerate what the pull request is for and about what it change, different **stages** as open or draft are given to theses requests, and people can get **assignements** to work on specific pull requests. These are also **committed multiple time** to keep them updated, and lead to a **public commentary section** for everyone to talk about the change and what they would or wouldn't do.








