# Discovering VSCODE 
![VScode](https://logowik.com/content/uploads/images/visual-studio-code7642.jpg)
click [here](https://code.visualstudio.com/) to check their official site.

| Title of the part | Description |
| ----------- | ----------- |
| What is VSCode |A short explanation about VSCode and why I chose to make my report about it  |
| The repo itself | A thorough description of the repo using some key numbers |
| PR and issues |A simple explanation as to how they manage issues and pull requests | 

## What is VScode?

VScode is a code editor featuring hundreds of language each coming with incredibly handy built-in tools for faster coding and a faster 
interface with git reposetries. The tools included go from an interactive debugger to snippets, highlights and much more. But that's not all 
of it, in fact the best part about this software is its high level of customizability,first with extensions going all the way to it being open-source. The project was launched by Microsoft in November 2015, and is now at version 1.65.

I personally chose to report on this repos, because we've used it a tiny bit during our discovery of git, but also because I have seen many 
of my friends that work in the field of IT use it, so I somewhat got curious about this mysterious tool.

## The repo itself

Since this software is an open source one, it is important to have a simple way to interract, add things, asks question and obviously this 
repos is providing with plenty of ways to do so. With around 1.6 thousand contributors for now, it is an astoundingly popular repo, managing 
to get 128 thousand stars (stars are a way to keep up with a repo, so in theory it basically shows how many people are interested in this repo). Interestingly enough they also use the project feature on this repo for a few of the issues, especially the ones related to the electron integration but this use has been overall scarce as only 4 were ever open, and 2 of them closed.

When it comes to extensions made by the public, these are dealt with through ecternal repos some of which are linked to on this main one. It seems somewhat obvious since extensions are not meant to be overviewed by the Microsoft staff.

In terms of planification, every single year the team in charge at Microsoft releases a roadmap every year as well as an iteration plan for approximately every month highlighting how much of the work they wanted to realise this month was done. For instance they explain in the most recent iteration plan made for their work on February that they wanted to work on several key points:
- Accessibility
- UX
- Workbench
- Web
- Code Editor
- Commenting 

... And many more with for each a simple sentence to describe every potential step to do. Those steps are labelled using emojis signifying the state they are in for instance 🏃 to explain that it's a work in progress.


## PR and issues

It comes without to say that it also implies a huge number of issues, 128 thousand to be precise with 5.4 thousand currently open. All of 
these issues are sorted using categories and tags. They also handle issues through stack overflow an external site.
In order for the moderators of the repo to accept pull requests, you have to sign a Contributor license agreement and link it to an issue or create an issue if your pull request wasn't tied to one. Moreover they give some indications on the way we may code or change things in general [here](https://github.com/microsoft/vscode/wiki/Coding-Guidelines). Mostly for every branch a community member creates, they mention the issue that's related to the changes they are making, aswell as their name.
