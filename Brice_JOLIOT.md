**JOLIOT Brice** <br/>
**SNUM 1** <br/>

# A new framework for Emacs users
![Emacs](https://raw.githubusercontent.com/emacs-eaf/emacs-application-framework/master/img/EAF_Banner_Transparent.png)
## Summary 

  1. Introduction
  2. Repository's description
  3. Pull Request


## Introduction

Emacs deals with a new issue. Emacs was created for more than 45 years ago, mostly its performances and graphics capabilities are limited 😞. That's why, there is a new project : [Emacs Application Framework](https://github.com/emacs-eaf/emacs-application-framework). It's a free and open-source extansible framework ! <br/>

I have chosen this [topic](https://github.com/emacs-eaf/emacs-application-framework) because I usually use emacs to program in C language 💻. I found that interested to choose a topic linked with emacs.


## Repository's description

### Main repository's numbers

| Number of contributors | Commits | Number of external documentation | Forks | Stars |
| ----------- | ----------- | ----------- | ----------- | ----------- |
| 66 | 803 | 2 | 174 | 2000 |

### Languages 💻
- [x] Emacs Lisp *41.3%*
- [x] Python *40.0*%
- [x] JavaScript *17.7%*
- [x] CSS *1.0%*

### External documentation used :
[EmacsConf2020 - Extend Emacs to Modern GUI Applications with EAF](https://www.example.com) <br/>
[EmacsConf2021 - Emacs Application Framework: A 2021 Update](https://www.youtube.com/watch?v=bh37zbefZk4) <br/>

```
There are other links, but these are github links and no docuementation.
``` 

## Pull request information
There are 545 pull request closed, so we can't see the various updates of the repository. However, we can always see the labels and the autors. <br/>
We can make a pull request, we just have to make a fork before. Also, there is the possibility for us to add an issue thanks to some templates and labels. <br/>
