Coronavirus Tracker API

+ what is it ?
+ how did that work ?
+ list of some commands
+ why is it a github repository ?

# what is it ?
  Coronavirus Tracker API is an API based on [<img src=https://www.python.org/static/img/python-logo.png align="top" height=25px width=100px>](https://www.python.org/downloads/release/python-330/)language and used by contributor in other languages (like PHP,C++ or java) to provide data about the covid19 outbreak all around the world.

  Those data are confirmed case, death, and recovery, but also information about the git repository like the number of contributors or the forks.

# how did that work ?
  First of all, you need to have python 3 and pipenv installed on you computer.
  
  If it done, you will have to clone the github repository of the API and copy the tracker with ` cd coronavirus-tracker-api ` then, with ` pipenv sync --dev`, you will open a virtual environment. Finally, with `pipenv shell`, you will open the pipenv shell where you will write your requests.

  The API used 3 different source for the data ([jhu](https://github.com/CSSEGISandData/COVID-19),[csbs](https://www.csbs.org/information-covid-19-coronavirus),[nyt](https://github.com/nytimes/covid-19-data)) thanks to that ,and with command in the pypenv shell, you can extract information you want.

# list of some commands
  `/v2/sources` give the name of the sources used.
|parameters|type|description|
|---|---|---|
|source | string \| optional | name of the source you want|

  `/v2/locations/` give the latest report per location.
|parameters|type|description|
|---|---|---|
|source | string \| optional | name of the source you want |
|id | int \| optional | id of the required country (can be found with `/v2/locations`) |
|country_code|string \| optional | ISO of the country you want |
|timeline|int \|optional | value between 0 and 1 to set the daily tracking | 

# why is it a github repository ?
This API was uploaded on github to be open to greater world due to the covid19 pandemic crisis. 

Thanks to this API, people stay informed about the crisis and other developer can use it to extend it in others languages to have access on other platforms, like mobile phone. in the other hands, people can make report of the API on the repository and the creator or other contributor can correct the problems
