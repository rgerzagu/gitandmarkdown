![image de bitwarden](bitwarden.png)
# Bitwarden 

[Bitwarden's officila website](https://bitwarden.com/)

Bitwarden is a password manager. It could generate high secure password, store and  syncronise each of them into your account. A particularity of bitwarden is that you can store each of your password on your own server without using bitwarden ones. I currently don't own a server, so I use bitwarden's server. There is a free version which provide an unlimited amount of password.






# Choice
I chose Bitwarden's project because I use it, and it could be great to know how Bitwarden's project is developed and to discover the Bitwarden's github.










# Look at Bitwarden's Github
Bitwarden has many repositories. For example there are : 
- server
- browser
- desktop
- mobile
- web

and many other.

When we go through a repositorie : [desktop](https://github.com/bitwarden/desktop) for example we can see the amount of branches, tags, the amount of issues or the amount of pull request. (At the date of 1/3/2022)

| branches | tags | issues | pull request | contributors |
|:----:|:---:|:---:|:---:|:--:|
| 22 | 102 | 185 | 15 | 65 |




We also can see the percent of each language in the code : 

| TypeScript | HTML | SCSS | JavaScript | PowerShell |
|:-:|:-:|:-:|:-:|:-:|
|47,4%|34,4%|12,5%|4,6%|1,1%|

In the tab insights we can see many statistics about the repositorie. We are able to see a graph which shows commits and another about comparison between addtions and deletions per week. It shows that there is more addtitions than deletions. 
In the same tab, we can see the dependency of the desktop's repositoy 


