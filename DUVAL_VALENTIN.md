###### DUVAL Valentin SNUM1


# [PlatformIO](https://platformio.org "PlatformIO official website") IDE: a [VSCode](https://code.visualstudio.com/ "VSCode official website") extension

![PlatformIO Logo](https://cdn.platformio.org/images/platformio-logo.17fdc3bc.png)

This essay is a report made for the evaluation of Software Development. This repository will be integrated as a branch in the [GitAndMarkdown](https://gitlab.enssat.fr/rgerzagu/gitandmarkdown) repository.

In this report, I will first present you what is PlatformIO and what it is used for. Then I'll describe the [PlatformIO-core](https://github.com/platformio/platformio-core) repository. Finally, I will explain how the Pull Requests and issues are managed.

---
## Summary
1. ### [What is PlatformIO](#what-is-platformio)
2. ### [Repository description](#repository-description)
3. ### [PR and issues](#pr-and-issues)
   
---

## :question: What is PlatformIO

PlatformIO is a VSCode extension for software developers. It gives them a modern integrated development environment that is cross-platform. Also, it facilitates the use of packages and libraries when it comes to using an other Software Development Kit. In addition, it includes a debugging tool, an unit testing that helps analyzing each part of a program, a code analysis that finds defects or even a tool to work remotely on a project.

>A native PlatformIO IDE extension for Microsoft VSCode editor is the most rated/reviewed extension with over 800 five-star reviews in the whole Microsoft Marketplace. It also was installed by over 750,000 unique developers around the world. [^1]

I have chosen this topic for my essay because VSCode is a software that I often use when it comes to developping code. Indeed, PlatformIO is an extension that could be useful in the future.

## :bookmark_tabs: Repository description

Some numbers about this repository:

| Collaborators | Commits | Forks | Stars | Releases | External Documentation |Language|
| ------------- | ------- | ----- | ----- | -------- | ---------------------- | ------- |
| 57 | 5651 | 661 | 5.7k | 111 | [Yes](https://docs.platformio.org/en/latest/)| Mainly Python |

## :exclamation: PR and issues

To open a new issue, there isn't any specific requirement. Someone will be assigned to the issue and will try to solve it.

To contribute to this repository, one needs to signe the Contributor License Agreement so that it can be used in the project.
Also, you need to follow some steps as shown in the [Contributing Guidelines](https://github.com/platformio/platformio-core/blob/develop/CONTRIBUTING.md). For example, you need to fork the repository and clone it. Moreover, when commiting you must make sure to test your code using `make test`.

[^1]: [PlatformIO documentation](https://docs.platformio.org/en/latest/what-is-platformio.html#philosophy)